import { Component } from '@angular/core';
import { push_mut, data } from '../home/home';
import { NavController, NavParams, Events } from 'ionic-angular';

@Component({
  templateUrl: 'tabs.html'
})


export class TabsPage {
  name: string = "Mælk"
  day: string = "01"
  month: string = "jan"
  year: string = "2019"
  constructor(public events: Events,public nav: NavController, public Params: NavParams) {
    if (Params.get("date")){
      var date : data = Params.get("date");
      this.day = Number(date.day) > 31 ? "31" : date.day;
      this.month = date.month;
      this.year = Number(date.year) > 2030 ? "2030" : Number(date.year) < 2019 ? "2019" : date.year;
    }
  }
  
  send(){
    push_mut(new data(this.name, this.day, this.month, this.year))
    this.events.publish('reloadPage1');
    this.nav.pop();
  }
}
