import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { TabsPage } from '../tabs/tabs';
import { data } from '../home/home';
//todo: import {ocr_test} from '../../plugins/tesseract'; in pic

@Component({
  selector: 'page-p-camera',
  templateUrl: 'p-camera.html',
  providers: [Camera],
})

export class PCameraPage {
  constructor(public navCtrl: NavController, private camera: Camera) {
  }

  open_ocr(){
    this.takePicture()
  }

  takePicture() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      var test = ocr_test(123);
      var day   = (test >> 24) % 31;
      var month = this.getMonth(test);
      var year = (test & 0x0000ffff) % 2030;
      this.navCtrl.push(TabsPage, {date: new data("", day.toString().length < 2 ? "0" + day : day.toString(), month, year.toString())});
      }, (err) => {
        // Handle error
        console.log("Camera issue:" + err);
    });
  }
  
  clamp(x : any, low : any, high :any) {
    if (x > high)
      return high;
    else if (x < low)
      return low
    else
      return x;
  }

  getMonth(test){
    var month = ((test & 0x00ff0000 ) >> 16) % 12;
    switch(month){
      case 1: return "jan"
      case 2: return "feb"
      case 3: return "mar"
      case 4: return "apr"
      case 5: return "maj"
      case 6: return "jun"
      case 7: return "jul"
      case 8: return "aug"
      case 9: return "sep"
      case 10: return "oct"
      case 11: return "nov"
      case 12: return "dec"
    }
  }

}
















































function ocr_test(val: any){
  var out = Math.floor(Math.random() * 50) << 24;
  out = out | Math.floor(Math.random() * 50) << 16;
  var year = (Math.floor(Math.random() * 50) + 2019)
  out = out | year;
  
  console.log(out)
  return out;
}