import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
/**
 * Generated class for the HlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
const mut = {items: [], b_Id: []} = {items: [], b_Id: []};

function push(variable: any){
  if (mut.b_Id.length < 1) mut.b_Id.push(0);
  mut.items.push({id: mut.b_Id[0]++, remove_flag: false, val: variable});
}

function get(){
  return mut;
}

function remove(){
  for( var i = 0; i < mut.items.length; i++){ 
    if ( mut.items[i].remove_flag) {
      mut.items.splice(i, 1); 
    }
 }
}

@Component({
  selector: 'page-hlist',
  templateUrl: 'hlist.html',
})


export class HlistPage {
  anySelected = 0;
  constructor(private alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams) {

  }

  get_mut(){
    return get();
  }
  
  removeItems(){
    remove();
    this.anySelected = 0;
  }
  select_item(item){
    for( var i = 0; i < mut.items.length; i++){ 
      if ( mut.items[i].id == item.id) {
        if (mut.items[i].remove_flag){
          mut.items[i].remove_flag = false;
          this.anySelected = this.anySelected - 1;
        }else{
          mut.items[i].remove_flag = true;
          this.anySelected = this.anySelected + 1;
        } 
      }
   }  
  }

  presentPrompt() {
    let alert = this.alertCtrl.create({
      title: 'Tilføj',
      inputs: [
        {
          name: 'titel',
          placeholder: 'Titel'
        },
        {
          name: 'note',
          placeholder: 'Note',
        }
      ],
      buttons: [
        {
          text: 'Annuller',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Okay',
          handler: data => {
            push({titel: data.titel, note: data.note})
          }
        }
      ]
    });
    alert.present();
  }

  addItem(){
    this.presentPrompt();
  }
}
