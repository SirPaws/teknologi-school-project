import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { PCameraPage } from '../p-camera/p-camera';
import { push_mut, data } from '../home/home';
@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})


export class AboutPage {
  constructor(public navCtrl: NavController, public navParam: NavParams) {
    console.log();
  }
  
  OpenCam(){
    this.navCtrl.push(PCameraPage);
  }

  addnew(){
    this.navCtrl.push(TabsPage);
  }

}

export var callByReference = function(varObj) { 
  console.log("Inside Call by Reference Method"); 
  varObj.a = 100; 
  console.log(varObj); 
} 
