import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { data, get_mut, remove_mut} from '../home/home';
import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
  public items = [];
  public mut : {array: data[] };
  anySelected = 0;

  constructor(public events: Events, public nav: NavController, public Params: NavParams) {
    this.events.subscribe('reloadPage1',() => {
      this.items = []
      this.update_list()
    });
  }
  
  ionViewDidLoad(){
    this.update_list()
  }

  update_list(){
    var d_fnc = new Date();
    
    var today_day   = d_fnc.getDate();
    var today_month = d_fnc.getMonth() + 1;
    var today_year  = d_fnc.getFullYear();
    
    console.log(""
    .concat(String(today_day))
    .concat("/").concat(String(today_month)).concat(" ").concat(String(today_year)))
    
    this.mut = get_mut();
      this.mut.array.forEach(element => {
        var val_day = Number(element.day);
        var val_month = this.month(element.month);
        var val_year = Number(element.year);

        console.log(""
        .concat(String(val_day))
        .concat("/").concat(String(val_month)).concat(" ").concat(String(val_year)))
            
        if (val_year - today_year == 0 ){
          if (val_month - today_month == 0){
            if (val_day - today_day <= 7){
              alert("Din ".concat(element.name).concat(" er ved at løbe ud!"))
            }
          }
        }
        
        if (val_year - today_year < 0)
          alert("Din ".concat(element.name).concat(" er ved at løbe ud!"))

        if (val_month - today_month < 0 && val_year - today_year == 0)
          alert("Din ".concat(element.name).concat(" er ved at løbe ud!"))

        var date = String(element.day).concat(" ").concat(element.month).concat(" ").concat(element.year);
        if (this.items == undefined){
          this.items = [{
          name: element.name, 
          date: date,
          id: element.id,
          selected: false
          }]
        }
        else{
          this.items.push({
            name: element.name, 
            date: date,
            id: element.id,
            selected: false
            })
        }
      });
  }

  addItem(){
    this.nav.push(TabsPage);
  }

  month(mnth: string){
    switch(mnth){
      case "jan": return 1
      case "feb": return 2
      case "mar": return 3
      case "apr": return 4
      case "maj": return 5
      case "jun": return 6
      case "jul": return 7
      case "aug": return 8
      case "sep": return 9
      case "oct": return 10
      case "nov": return 11
      case "dec": return 12
    }
  }

  removeItems(){
    for( var j = 0; j < this.anySelected; j++){
      for( var i = 0; i < this.items.length; i++){ 
        if ( this.items[i].selected) {
          remove_mut(this.items[i].id);
          this.items.splice(i, 1); 
        }
     }
    }
    this.anySelected = 0;
  }
  
  viewItem(item){
    console.log(item);
    if (item.selected){
      item.selected = false;
      this.anySelected--;
    }
    else{
      item.selected = true;
      this.anySelected++;
    }
  }

  

}
