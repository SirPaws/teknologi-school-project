import { Component } from '@angular/core';
import { NavController, NavPush } from 'ionic-angular';
import { AboutPage } from '../about/about'
import { ContactPage } from '../contact/contact'
import { InfoPage } from '../info/info'
import { HlistPage } from '../hlist/hlist'

const mutable : { array: data[] } = {array: []};
var id = 0;

export function push_mut(variable: data){
  variable.id = id++;
  mutable.array.push(variable);
}

export function get_mut(){
  return mutable;
}

export function remove_mut(index : any){
  for( var i = 0; i < mutable.array.length; i++){ 
    if ( mutable.array[i].id == index) {
      mutable.array.splice(i, 1); 
    }
 }
}

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

//TODO: A page called "Omkring Data" that tells the user what data will be collected  

export class HomePage {
  constructor(public navCtrl: NavController, ) {
    mutable.array.forEach(element => {
      console.log(element)
    });
  }
  
  log(){
    if (mutable.array){
      mutable.array.forEach(element => {
        console.log(element)
      });
    }
  }
  
  openList_h(){
    this.navCtrl.push(HlistPage)
  }

  openList_items(){
    this.navCtrl.push(ContactPage)
  }

  openScan(){
    this.navCtrl.push(AboutPage)
  }
  
  openInfo(){
    this.navCtrl.push(InfoPage)
  }
}

export class data{
  constructor(public name: string = "", 
  public day: string = "01", public month: string = "jan", public year: string = "2019", 
  public id : Number = 0){

  }
}